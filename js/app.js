$(document).ready(function () {

    const localStorageKey = "GTFO_helper_localStorage_key";
    let session;

    init();

    /** Restart new session by reset the local storage. */
    $('#resetLocalStorage').click(function () {
        localStorage.removeItem(localStorageKey);
        init()
    });

    $('#addObject').click(function () {
        const rawId = generateId();

        storeRow({
            objectType: $('#inputType').val(),
            rawId: rawId
        });

        loadLocalStorageSession();
    });

    function init() {
        if (localStorage.getItem(localStorageKey) === null) {
            localStorage.setItem(localStorageKey, JSON.stringify({
                id: generateId(),
                objects: []
            }));
        }

        loadLocalStorageSession();
    }

    function loadLocalStorageSession() {
        session = JSON.parse(localStorage.getItem(localStorageKey));

        refreshCurrentLocalStorageUsage();
        refreshCurrentSessionId();
        refreshTable();
    }

    function refreshCurrentSessionId() {
        $('#currentSessionId').text(function () {
            return session.id;
        });
    }

    function refreshCurrentLocalStorageUsage() {
        $('#currentLocalStorageUsage').text(function () {
            return formatBytes(stringToArrayBuffer(JSON.stringify(session)).byteLength);
        });
    }

    function refreshTable() {
        $('#table > tbody').empty();

        if (session.objects.length > 0) {
            session.objects
                .sort(function (first, second) {
                    let sortFunction = naturalSort();

                    return sortFunction(first.objectType, second.objectType);
                })
                .forEach(function (object) {
                    addRow(object);
                });
        }
    }

    function storeRow(object) {
        session.objects.push(object);

        storeSession();
    }

    function addRow(object) {
        const idInputId = "id" + object.rawId;
        const tableRowId = "row" + object.rawId;
        const zoneInputId = "zone" + object.rawId;
        const areaInputId = "area" + object.rawId;

        const foundButtonId = "foundButton" + object.rawId;
        const deleteButtonId = "deleteButton" + object.rawId;

        $('#table > tbody:last-child').append('<tr id="' + tableRowId + '">\n' +
            '                            <td><span>' + object.objectType + '</span></td>\n' +
            '                            <td>\n' +
            '                                <input id="' + idInputId + '" type="text"/>\n' +
            '                            </td>\n' +
            '                            <td>\n' +
            '                                <input id="' + zoneInputId + '" type="text" value="' + object.zoneInputValue + '"/>\n' +
            '                            </td>\n' +
            '                            <td>\n' +
            '                                <input id="' + areaInputId + '" type="text" value="' + object.areaInputValue + '"/>\n' +
            '                            </td>\n' +
            '                            <td>\n' +
            '                                <input type="button" id="' + foundButtonId + '" value="Found" class="btn btn-info">\n' +
            '                                <input type="button" id="' + deleteButtonId + '" value="Delete" class="btn btn-danger">\n' +
            '                            </td>\n' +
            '                        </tr>');

        const tableRowElem = $('#' + tableRowId);
        if (object.found !== undefined && object.found) {
            tableRowElem.addClass("found");
        }
        $('#' + foundButtonId).click(function () {

            if (tableRowElem.hasClass("found")) {
                tableRowElem.removeClass("found");
            } else {
                tableRowElem.addClass("found");
            }

            object.found = !object.found;

            storeSession();
        });

        $('#' + deleteButtonId).click(function () {
            tableRowElem.empty();

            let newSessionObjects = [];
            session.objects
                .filter(sessionObject => sessionObject.rawId !== object.rawId)
                .forEach(sessionObject => newSessionObjects.push(sessionObject));

            session.objects = newSessionObjects;
            storeSession();
        });

        const idInputIdElem = $('#' + idInputId);
        idInputIdElem.val(object.idInputValue);
        idInputIdElem.change(function () {
            session.objects.forEach(function (sessionObject) {
                if ("id" + sessionObject.rawId === idInputId) {
                    sessionObject.idInputValue = idInputIdElem.val();

                    storeSession();
                }
            });
        });

        const zoneInputIdElem = $('#' + zoneInputId);
        zoneInputIdElem.val(object.zoneInputValue);
        zoneInputIdElem.change(function () {
            session.objects.forEach(function (sessionObject) {
                if ("zone" + sessionObject.rawId === zoneInputId) {
                    sessionObject.zoneInputValue = zoneInputIdElem.val();

                    storeSession();
                }
            });
        });

        const areaInputIdElem = $('#' + areaInputId);
        areaInputIdElem.val(object.areaInputValue);
        areaInputIdElem.change(function () {
            session.objects.forEach(function (sessionObject) {
                if ("area" + sessionObject.rawId === areaInputId) {
                    sessionObject.areaInputValue = areaInputIdElem.val();

                    storeSession();
                }
            });
        });
    }

    function storeSession() {
        localStorage.setItem(localStorageKey, JSON.stringify(session));
    }

    function formatBytes(bytes, decimals = 2) {
        if (bytes === 0) {
            return '0 Bytes';
        }

        const k = 1024;
        const dm = decimals < 0 ? 0 : decimals;
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

        const i = Math.floor(Math.log(bytes) / Math.log(k));

        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    }

    function stringToArrayBuffer(str) {
        let buf = new ArrayBuffer(str.length * 2); // 2 bytes for each char
        let bufView = new Uint16Array(buf);
        for (let i = 0, strLen = str.length; i < strLen; i++) {
            bufView[i] = str.charCodeAt(i);
        }
        return buf;
    }

    function generateId() {
        return '_' + Math.random().toString(36).substr(2, 9);
    }

    function naturalSort() {
        return function (first, second) {
            return first.localeCompare(second);
        };
    }

    // Cannot use external JSON file because of $.getJson CORS issue
    JSON.parse("[\n" +
        "  \"HDD\",\n" +
        "  \"FOG_TURBINE\",\n" +
        "  \"DISINFECT_STATION\",\n" +
        "  \"CRYO\",\n" +
        "  \"BULKHEAD_KEY\",\n" +
        "  \"SEC_DOOR\",\n" +
        "  \"BOX\",\n" +
        "  \"LOCKER\",\n" +
        "  \"TOOL_REFILL\",\n" +
        "  \"MEDIPACK\",\n" +
        "  \"AMMOPACK\",\n" +
        "  \"TERMINAL\",\n" +
        "  \"CELL\",\n" +
        "  \"GENERATOR\",\n" +
        "  \"BULKHEAD_DC\",\n" +
        "  \"DOOR\"\n" +
        "]")
        .sort(naturalSort())
        .forEach(function (value) {
            $('#inputType').append($('<option>', {
                value: value,
                text: value
            }));
        });
});
